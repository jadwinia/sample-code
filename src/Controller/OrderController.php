<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Exception\NoWebsiteException;
use App\Service\ItemManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\OrderManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @author Justyna Jurczuk <jjurczuk@weby.pl>
 */
class OrderController extends AbstractController
{

    /**
     * @return Response
     */
    public function showExampleItemsAction(): Response
    {
        $itemManager = $this->get(ItemManager::class);
        $items = $itemManager->getExampleItems();

        return $this->render('order/index.html.twig', ['items' => $items]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addItemAction(Request $request): Response
    {
        $itemsIdsArray = $request->request->get('items');
        $itemManager = $this->get(ItemManager::class);
        $orderManager = $this->get(OrderManager::class);

        foreach ($itemsIdsArray as $id) {
            $orderManager->addItem($itemManager->getItemDTOById($id));
        }

        return $this->render('order/index.html.twig', []);
    }

    /**
     * @return Response
     */
    public function saveOrderAction(): Response
    {
        try {
            $this->get(OrderManager::class)->saveOrder();
            $message = "Order saved.";
        } catch (NoWebsiteException $e) {
            $message = $e->getMessage();
        } catch (\Exception $e) {
            $message = "Somethig went wrong. Order not saved.";
        }

        return $this->render('order/index.html.twig', ['message' => $message]);
    }

}
