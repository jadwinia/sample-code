<?php
declare(strict_types = 1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Justyna Jurczuk <jjurczuk@weby.pl>
 */
class ItemRepository extends EntityRepository
{
}
