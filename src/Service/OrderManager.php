<?php
declare(strict_types = 1);

namespace App\Service;


use App\Factory\OrderDTOFactory;
use App\Model\DTO\ItemDTO;
use App\Model\DTO\OrderDTO;
use App\Transformer\OrderTransformer;
use Doctrine\ORM\EntityManager;


/**
 * @author Justyna Jurczuk <jjurczuk@weby.pl>
 */
class OrderManager
{
    /**
     * @var OrderDTO
     */
    private $order;

    /**
     * @var SessionManager
     */
    private $sessionManager;

    /**
     * @var OrderDTOFactory
     */
    private $orderDTOFactory;

    /**
     * @var OrderTransformer
     */
    private $orderTransformer;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * OrderManager constructor.
     * @param SessionManager $sessionManager
     * @param OrderDTOFactory $orderDTOFactory
     * @param OrderTransformer $orderTransformer
     * @param EntityManager $em
     */
    public function __construct(
        SessionManager $sessionManager,
        OrderDTOFactory $orderDTOFactory,
        OrderTransformer $orderTransformer,
        EntityManager $em
    )
    {
        $this->sessionManager = $sessionManager;
        $this->orderDTOFactory = $orderDTOFactory;
        $this->orderTransformer = $orderTransformer;
        $this->em = $em;
    }

    /**
     * @return OrderDTO
     */
    public function getOrder(): OrderDTO
    {
        if (null === $this->order) {
            $this->setOrder();
        }

        return $this->order;
    }

    /**
     * @param ItemDTO $itemDTO
     */
    public function addItem(ItemDTO $itemDTO): void
    {
        $orderDTO = $this->getOrder();
        $orderDTO->addItem($itemDTO);
        $orderDTO->setTotalPrice($orderDTO->getTotalPrice() + $itemDTO->getPrice());
    }

    /**
     * @param OrderDTO $order
     * @return int
     */
    public function getNumberOfItems(OrderDTO $order): int
    {
        return count($order->getItems());
    }

    public function saveOrder(): void
    {
        $order = $this->orderTransformer->transformOrderDTOToEntity($this->getOrder());
        $this->em->persist($order);
        $this->em->flush();
        $this->sessionManager->unsetOrder();
    }

    private function setOrder(): void
    {
        $this->order = $this->orderDTOFactory->create();
        $this->sessionManager->setOrder($this->order);
    }
}
