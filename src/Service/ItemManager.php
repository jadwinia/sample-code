<?php
declare(strict_types = 1);

namespace App\Service;


use App\Entity\Item;
use App\Factory\ItemDTOFactory;
use App\Model\DTO\ItemDTO;
use App\Repository\ItemRepository;
use App\Transformer\ItemTransformer;
use Doctrine\ORM\EntityManager;

/**
 * @author Justyna Jurczuk <jjurczuk@weby.pl>
 */
class ItemManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ItemTransformer
     */
    private $itemTransformer;

    /**
     * @var ItemDTOFactory
     */
    private $itemDTOFactory;

    /**
     * @var array
     * Used just for an example - there is no database
     */
    private $exampleData = [
        'product_1' => 10000,
        'product_2' => 20000
    ];

    /**
     * ItemManager constructor.
     * @param EntityManager $em
     * @param ItemDTOFactory $itemDTOFactory
     */
    public function __construct(EntityManager $em, ItemDTOFactory $itemDTOFactory)
    {
        $this->em = $em;
        $this->itemDTOFactory = $itemDTOFactory;
    }

    /**
     * @param int $id
     * @return ItemDTO
     */
    public function getItemDTOById(int $id): ItemDTO
    {
        $item = $this->getItemRepo()->find($id);
        $itemDTO = $this->itemTransformer->transformEntityToItemDTO($item);

        return $itemDTO;
    }

    /**
     * @return array
     * Used just for an example - there is no database
     */
    public function getExampleItems(): array
    {
        $items = [];
        $id = 1;
        foreach ($this->exampleData as $key => $value) {
            $itemDTO = $this->itemDTOFactory->create($id, $key, $value);
            $items[] = $itemDTO;
            $id++;
        }

        return $items;
    }

    /**
     * @return ItemRepository
     */
    private function getItemRepo(): ItemRepository
    {
        return $this->em->getRepository(Item::class);
    }

}