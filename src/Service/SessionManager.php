<?php
declare(strict_types = 1);

namespace App\Service;


use App\Model\DTO\OrderDTO;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @author Justyna Jurczuk <jjurczuk@weby.pl>
 */
class SessionManager
{
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @param OrderDTO $orderDTO
     */
    public function setOrder(OrderDTO $orderDTO): void
    {
        $this->session->set('order', $orderDTO);
    }

    public function unsetOrder(): void
    {
        $this->session->remove('order');
    }
}
