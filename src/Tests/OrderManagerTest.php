<?php
declare(strict_types = 1);

namespace App\Tests;

use App\Factory\ItemDTOFactory;
use App\Factory\OrderDTOFactory;
use App\Model\DTO\ItemDTO;
use App\Service\OrderManager;
use App\Service\SessionManager;
use App\Transformer\ItemTransformer;
use App\Transformer\OrderTransformer;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @author Justyna Jurczuk <jjurczuk@weby.pl>
 */
class OrderManagerTest extends TestCase
{
    public function testAddItem(): void
    {
        $orderManager = $this->getOrderManager();
        $orderManager->addItem($this->getExampleItem());
        $order = $orderManager->getOrder();

        $this->assertNotNull($order);
        $this->assertNotEmpty($order->getItems());
        $this->assertEquals(130, $order->getTotalPrice());
    }

    private function getOrderManager(): OrderManager
    {
        $emMock = $this->createMock(EntityManager::class);
        $sessionMock = $this->createMock(SessionInterface::class);
        $sessionManager = new SessionManager($sessionMock);
        $itemDTOFactory = new ItemDTOFactory();
        $itemTransformer = new ItemTransformer($itemDTOFactory);
        $orderDTOFactory = new OrderDTOFactory('test-website');
        $orderTransformer = new OrderTransformer($itemTransformer, $orderDTOFactory);

        $orderManager = new OrderManager(
            $sessionManager,
            $orderDTOFactory,
            $orderTransformer,
            $emMock
        );

        return $orderManager;
    }

    private function getExampleItem(): ItemDTO
    {
        $itemDTOFactory = new ItemDTOFactory();

        return $itemDTOFactory->create(1, 'casual name', 130);
    }
}
