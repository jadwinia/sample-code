<?php
declare(strict_types = 1);

namespace App\Exception;

/**
 * @author Justyna Jurczuk <jjurczuk@weby.pl>
 */
class NoWebsiteException extends \Exception
{
}